Matrix D&D bot
==============

This is currently just a functional concept.

### Features:

- dice roll: `!roll 1d20`, `!roll 5d6`

### Future features:
- custom named rolls per user
- integration of basic charsheet functions and  generated rolls

### Configuration
- copy `config/default.toml` to `config/user.toml`
- edit default values
- password should be passed by env variable `dragon_matrix_password`