use futures::TryStreamExt;

use ruma_client_api::r0::sync;
use ruma_client_api::r0::sync::sync_events::SetPresence;
use std::time::Duration;

mod lib;
use lib::db::DbKeys;

#[macro_use]
extern crate lazy_static;

#[tokio::main]
async fn main() {
    let config = match lib::settings::Settings::new() {
        Ok(config) => config,
        Err(e) => {
            println!("{}", e);
            panic!()
        }
    };

    let mut db = lib::db::connect_to_db(&config.db);

    let client = lib::connection::connect(db.get(DbKeys::Session.as_str()), &config.matrix).await;

    if let Err(e) = db.set(DbKeys::Session.as_str(), &client.session()) {
        println!("Can't save session: {}", e)
    }

    let own_mxid = client.session().unwrap().identification.unwrap().user_id;

    let filter = lib::helpers::generate_filter(&own_mxid).ok();

    let next_batch: Option<String> = db.get(DbKeys::NextBatch.as_str()).unwrap_or(Some(
        client
            .request(sync::sync_events::Request {
                filter: filter.clone(),
                since: None,
                full_state: false,
                set_presence: SetPresence::Unavailable,
                timeout: None,
            })
            .await
            .expect("Can't sync initial batch")
            .next_batch,
    ));
    let mut sync_stream = Box::pin(client.sync(
        lib::helpers::generate_filter(&own_mxid).ok(),
        next_batch,
        SetPresence::Online,
        Some(Duration::from_secs(30)),
    ));

    while let Some(response) = sync_stream.try_next().await.unwrap() {
        let response: ruma_client::api::r0::sync::sync_events::Response = response;
        lib::event_processor::process_room_events(response.rooms.join, &client).await;
        if let Err(e) = db.set(DbKeys::NextBatch.as_str(), &response.next_batch.as_str()) {
            println!("Can't save last batch: {}", e)
        }
    }
}
