use pickledb::{PickleDb, PickleDbDumpPolicy, SerializationMethod};

pub fn connect_to_db(db_config: &crate::lib::settings::Db) -> PickleDb {
    let db_path = &db_config.file.to_string();
    PickleDb::load(
        db_path,
        PickleDbDumpPolicy::AutoDump,
        SerializationMethod::Json,
    )
    .unwrap_or(PickleDb::new(
        db_path,
        PickleDbDumpPolicy::AutoDump,
        SerializationMethod::Json,
    ))
}

pub enum DbKeys {
    Session,
    NextBatch,
}

impl DbKeys {
    pub fn as_str(&self) -> &'static str {
        match self {
            DbKeys::Session => "session",
            DbKeys::NextBatch => "next_batch",
        }
    }
}
