use crate::lib::event_builders::EventFilterBuilder;
use ruma_client_api::r0::filter::{Filter, FilterDefinition, RoomEventFilter, RoomFilter};
use ruma_client_api::r0::sync::sync_events;
use ruma_events::EventType;
use ruma_identifiers::UserId;

pub fn generate_filter(own_id: &UserId) -> anyhow::Result<sync_events::Filter> {
    let filter = FilterDefinition {
        event_fields: None,
        event_format: None,
        account_data: Some(Filter::ignore_all()),
        room: Some(RoomFilter {
            include_leave: None,
            account_data: Some(RoomEventFilter::ignore_all()),
            timeline: Some(
                RoomEventFilter::new()
                    .not_sender(own_id)
                    .add_type(EventType::RoomMessage)
                    .clone(),
            ),
            ephemeral: Some(RoomEventFilter::ignore_all()),
            state: Some(RoomEventFilter::ignore_all()),
            not_rooms: vec![],
            rooms: None,
        }),
        presence: Some(Filter::ignore_all()),
    };

    Ok(sync_events::Filter::FilterDefinition(filter))
}
