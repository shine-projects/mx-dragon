use ruma_client::{Client, HttpsClient, Session};

pub async fn connect(
    session: Option<Session>,
    matrix_settings: &crate::lib::settings::Matrix,
) -> HttpsClient {
    let homeserver_url = matrix_settings
        .homeserver
        .parse()
        .expect(format!("Wrong url format: {}", matrix_settings.homeserver).as_str());

    let client = Client::https(homeserver_url, session);
    if let None = client.session() {
        println!("Connecting for a first time");
        let session = client
            .log_in(
                matrix_settings.user.to_string(),
                matrix_settings.password.to_string(),
                Some("dragon-lair".to_string()),
                None,
            )
            .await;
        if let Err(e) = session {
            panic!("Can't connect: {}", e)
        };
    }

    client
}
