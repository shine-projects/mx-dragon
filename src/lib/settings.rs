use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Db {
    pub file: String,
}

#[derive(Debug, Deserialize)]
pub struct Matrix {
    pub user: String,
    pub homeserver: String,
    pub password: String,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub db: Db,
    pub matrix: Matrix,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::new();
        s.merge(File::with_name("config/default.toml"))?;
        s.merge(File::with_name("config/user.toml").required(false))?;
        s.merge(Environment::with_prefix("dragon").separator("_"))?;
        s.try_into()
    }
}
