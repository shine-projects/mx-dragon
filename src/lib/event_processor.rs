use rand::prelude::ThreadRng;
use rand::Rng;
use regex::Regex;
use ruma_client::HttpsClient;
use ruma_client_api::r0::message::create_message_event;
use ruma_client_api::r0::sync::sync_events::JoinedRoom;
use ruma_events::collections::all::RoomEvent;
use ruma_events::room::message::MessageEventContent::Text;
use ruma_events::room::message::{
    EmoteMessageEventContent, InReplyTo, MessageEvent, MessageEventContent, RelatesTo,
    TextMessageEventContent,
};
use ruma_events::EventType;
use ruma_identifiers::{EventId, RoomId};
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::ops::DerefMut;
use std::str::FromStr;
use uuid::Uuid;

thread_local! {
    static RNG: RefCell<ThreadRng> = RefCell::new(rand::thread_rng());
}

pub async fn process_room_events(rooms: BTreeMap<RoomId, JoinedRoom>, client: &HttpsClient) {
    for (room_id, room) in rooms {
        let timeline = room.timeline;
        for event in timeline.events {
            if let Ok(event) = event.deserialize() {
                if let RoomEvent::RoomMessage(message) = event {
                    if let Text(text_event) = &message.content {
                        process_command(client, &room_id, &message, text_event).await
                    }
                }
            }
        }
    }
}

fn parse_num<T>(s: &str) -> anyhow::Result<T>
where
    T: FromStr,
{
    match s.parse::<T>() {
        Ok(parsed) => Ok(parsed),
        Err(_) => Err(anyhow::Error::msg("Failed to parse value")),
    }
}

fn parse_defaulted_num<T>(s: &str, default: T) -> anyhow::Result<T>
where
    T: FromStr,
{
    if s == "" {
        return Ok(default);
    }
    parse_num(s)
}

async fn process_command(
    client: &HttpsClient,
    room_id: &RoomId,
    message: &MessageEvent,
    text_event: &TextMessageEventContent,
) {
    lazy_static! {
        static ref RE_DICE: Regex = Regex::new(r"!roll\s+(\d*)d(\d+)").unwrap();
    }

    if let Some(roll) = RE_DICE.captures(&text_event.body) {
        let dice_num = parse_defaulted_num(&roll[1], 1);
        let dice_type: anyhow::Result<u64> = parse_num(&roll[2]);

        if dice_num.is_err() || dice_type.is_err() {
            send_emote(
                client,
                room_id,
                format!(
                    "bites {}'s hand for trying to hurt him with large numbers",
                    &message.sender
                ),
            )
            .await;
            return;
        }
        let dice_num = dice_num.unwrap();
        let dice_type = dice_type.unwrap();

        if dice_num > 128 {
            send_message(
                client,
                room_id,
                &message.event_id,
                format!("You shouldn't use so many dice. The limit is 128, surely that's enough."),
            )
            .await;
            return;
        }
        if dice_type == 0 {
            send_message(
                client,
                room_id,
                &message.event_id,
                format!("Oh come on! 0 sided dice? You are kidding, right?."),
            )
            .await;
            return;
        }
        // let dice_type: u64 = dice_type.unwrap();
        let result: Option<u64> = std::iter::repeat(())
            .take(dice_num)
            .fold(Some(0), |sum, _| {
                let sum = sum.unwrap();
                sum.checked_add(RNG.with(|r| r.borrow_mut().deref_mut().gen_range(1, dice_type)))
            });
        if let Some(result) = result {
            send_message(
                client,
                room_id,
                &message.event_id,
                format!("Rolled {}d{} with result: {}", dice_num, dice_type, result),
            )
            .await;
        } else {
            send_message(
                client,
                room_id,
                &message.event_id,
                "The size of your roll exceeded my capability to count it, you just won everything"
                    .to_string(),
            )
            .await;
        }
    }
}

async fn send_message(client: &HttpsClient, room_id: &RoomId, message_id: &EventId, text: String) {
    client
        .request(create_message_event::Request {
            room_id: room_id.clone(),
            event_type: EventType::RoomMessage,
            txn_id: Uuid::new_v4().to_string(),
            data: serde_json::value::to_raw_value(&MessageEventContent::Text(
                TextMessageEventContent {
                    body: text,
                    format: None,
                    formatted_body: None,
                    relates_to: Some(RelatesTo {
                        in_reply_to: InReplyTo {
                            event_id: message_id.clone(),
                        },
                    }),
                },
            ))
            .unwrap(),
        })
        .await
        .unwrap();
}

async fn send_emote(client: &HttpsClient, room_id: &RoomId, text: String) {
    client
        .request(create_message_event::Request {
            room_id: room_id.clone(),
            event_type: EventType::RoomMessage,
            txn_id: Uuid::new_v4().to_string(),
            data: serde_json::value::to_raw_value(&MessageEventContent::Emote(
                EmoteMessageEventContent {
                    body: text,
                    format: None,
                    formatted_body: None,
                },
            ))
            .unwrap(),
        })
        .await
        .unwrap();
}
