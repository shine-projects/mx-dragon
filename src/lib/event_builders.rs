use ruma_client_api::r0::filter::RoomEventFilter;
use ruma_events::EventType;
use ruma_identifiers::UserId;

pub trait EventFilterBuilder {
    fn new() -> RoomEventFilter;
    fn add_type(&mut self, event_type: EventType) -> &mut RoomEventFilter;
    fn set_types(&mut self, event_types: Vec<EventType>) -> &mut RoomEventFilter;
    fn limit(&mut self, limit: u32) -> &mut RoomEventFilter;
    fn not_sender(&mut self, sender: &UserId) -> &mut RoomEventFilter;
}

impl EventFilterBuilder for RoomEventFilter {
    fn new() -> RoomEventFilter {
        RoomEventFilter {
            not_types: vec![],
            not_rooms: vec![],
            limit: None,
            rooms: None,
            not_senders: vec![],
            senders: None,
            types: None,
            contains_url: None,
            lazy_load_options: Default::default(),
        }
    }

    fn add_type(&mut self, event_type: EventType) -> &mut RoomEventFilter {
        if let Some(types) = self.types.as_mut() {
            types.push(event_type.to_string())
        } else {
            self.types = Some(vec![event_type.to_string()])
        }
        self
    }

    fn set_types(&mut self, event_types: Vec<EventType>) -> &mut RoomEventFilter {
        self.types = Some(
            event_types
                .into_iter()
                .map(|event_type| event_type.to_string())
                .collect(),
        );
        self
    }

    fn limit(&mut self, limit: u32) -> &mut RoomEventFilter {
        self.limit = Some(js_int::UInt::from(limit));
        self
    }

    fn not_sender(&mut self, sender: &UserId) -> &mut RoomEventFilter {
        self.not_senders.push(sender.clone());
        self
    }
}
